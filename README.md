# Multi Purpose Web Board
A platform for every small project out there with the need for a small GUI.

## Setup for developing
```
# Install npm for your platform
?

# Install vue cli tool (highly recommended)
npm install -g @vue/cli

# Start dev gui
cd web-mpwdboard/
vue ui
```

export default class Globals {
    public static API_URL__NAS_BROWSER = 'https://localhost:404/';
    public static API_URL__YTDL_DIRECT_LINK_EXTRACTOR = 'https://pollux-server.de:443/ytdl/video_info.php';
    public static API_URL__YTDL_CONVERTER = 'https://pollux-server.de:443/media/index.php';
}
